import sys
import argparse
import threading
import socket
from pyrad.client import Client
from pyrad.dictionary import Dictionary
import pyrad.packet
import random
import queue
import time
from progress.bar import Bar
import logging
from pyfiglet import Figlet
from DataAuthResponse import DataAuthResponseClass

#  python3 main.py -ip 3.22.242.31 -thread 10 -username 4434579619D6LC5W4N32
#  -password 4434579619D6LC5W4N32 -r 1000 -type auth -secret space -timeout 500

logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-s) %(message)s')


def accountingTestFunction(ip, secret, username, password, request):
    for i in range(int(request)):
        srv = Client(server=ip, secret=bytes(secret, encoding='utf8'), dict=Dictionary("dictionary"))

        req = srv.CreateAcctPacket(User_Name=username, User_Password=password)

        req["NAS-IP-Address"] = "192.168.1.25"
        req["NAS-Port"] = "1812"
        req["NAS-Identifier"] = "trillian"
        req["Called-Station-Id"] = "00-04-5F-00-0F-D1"
        req["Calling-Station-Id"] = "00-01-24-80-B3-9C"
        req["Framed-IP-Address"] = "10.0.0.100"

        print("Sending accounting start packet")
        req["Acct-Status-Type"] = "Start"
        sendPacket(srv, req)

        print("Sending accounting stop packet")
        req["Acct-Status-Type"] = "Stop"
        req["Acct-Input-Octets"] = random.randrange(2 ** 10, 2 ** 30)
        req["Acct-Output-Octets"] = random.randrange(2 ** 10, 2 ** 30)
        req["Acct-Session-Time"] = random.randrange(120, 3600)
        req["Acct-Terminate-Cause"] = random.choice(["User-Request", "Idle-Timeout"])
        sendPacket(srv, req)


def sendPacket(srv, req):
    try:
        srv.SendPacket(req)
        return 0
    except pyrad.client.Timeout:
        return 1
    except socket.error as error:
        return 2


# Status code
# 0 Success
# 1 Failed timeout
# 2 Failed
# 3 Radius Server not reply
# 4 Network Error
def authTestFunctionLoop(username, password, ip, secret, timeout, request, verbosemode):
    response = []
    for i in range(int(request)):
        response.append(authTestFunction(username, password, ip, secret, timeout, i, verbosemode))
    return response


def authTestFunction(username, password, ip, secret, timeout, i, verbosemode):
    srv = Client(server=ip, secret=bytes(secret, encoding='utf8'), dict=Dictionary("dictionary"))

    req = srv.CreateAuthPacket(code=pyrad.packet.AccessRequest, User_Name=username,
                               User_Password=password)

    req["NAS-IP-Address"] = "192.168.1.10"
    req["NAS-Port"] = 0
    req["Service-Type"] = "Login-User"
    req["NAS-Identifier"] = "trillian"
    req["Called-Station-Id"] = "00-04-5F-00-0F-D1"
    req["Calling-Station-Id"] = "00-01-24-80-B3-9C"
    req["Framed-IP-Address"] = "10.0.0.100"

    try:
        reply = srv.SendPacket(req)
    except pyrad.client.Timeout:
        if verbosemode:
            logging.info("Request " + str(i + 1) + " Timeout Error ")
        return DataAuthResponseClass(i, 3, threading.get_ident())
    except socket.error as error:
        if verbosemode:
            logging.info("Request " + str(i + 1) + " Error Socket ")
        return DataAuthResponseClass(i, 4, threading.get_ident())

    if reply.code == pyrad.packet.AccessAccept:
        if reply["Idle-Timeout"] == timeout or reply["Idle-Timeout"] < timeout:
            if verbosemode:
                logging.info("Request " + str(i + 1) + " Success in " + reply["Idle-Timeout"])
            return DataAuthResponseClass(i, 0, threading.get_ident())
        else:
            if verbosemode:
                logging.info("Request " + str(i + 1) + " Error in " + reply["Idle-Timeout"]) + " is more than " + str(
                    timeout)
            return DataAuthResponseClass(i, 1, threading.get_ident())
    else:
        if verbosemode:
            logging.info("Request " + str(i + 1) + " Network Error ")
        return DataAuthResponseClass(i, 2, threading.get_ident())


def createArguments():
    parser = argparse.ArgumentParser(
        description='Radius server testing tool',
    )
    parser.add_argument('-ip', help="IP address of the server", required=True)
    parser.add_argument('-thread', help="Number of threads", default=1, type=int, required=True)
    parser.add_argument('-username', help="Username", required=True)
    parser.add_argument('-password', help="Password", required=True)
    parser.add_argument('-request', help="Number of Request", type=int, required=True)
    parser.add_argument('-timeout', help="Timeout", type=float, required=True)
    parser.add_argument('-type', help="Test Accounting or Authentication", required=True)
    parser.add_argument('-secret', help="Secret", required=True)
    parser.add_argument('-verbose', help="Show Verbose", default=False, type=bool)
    # parser.add_argument('-port', help="PORT", default=1812)
    return parser.parse_args()


def startData(numbRequest):
    global sucess
    global failed
    global radiusError
    global networkError
    sys.stdout.write(" Sucess {} of {} / {:.0%} \n".format(0, numbRequest, 0))
    sys.stdout.write(" Error  {} of {} / {:.0%} \n".format(0, numbRequest, 0))
    sys.stdout.write(" Radius Error  {} of {} / {:.0%} \n".format(0, numbRequest, 0))
    sys.stdout.write(" Network Error  {} of {} / {:.0%} \n".format(0, numbRequest, 0))


def processData(data):
    sucess = 0
    failed = 0
    radiusError = 0
    networkError = 0
    for ls in data:
        for i in ls:
            if getattr(i, 'response') == 0:
                sucess = sucess + 1
            elif getattr(i, 'response') == 1:
                failed = failed + 1
            elif getattr(i, 'response') == 2:
                radiusError = radiusError + 1
            else:
                networkError = networkError + 1
    print()
    print("Result")
    print(" Sucess {} of {} / {:.0%} ".format(sucess, len(data), sucess / len(data)))
    print(" Error  {} of {} / {:.0%}".format(failed, len(data), failed / len(data)))
    print(" Radius Error  {} of {} / {:.0%}".format(radiusError, len(data), radiusError / len(data)))
    print(" Network Error  {} of {} / {:.0%}".format(networkError, len(data), networkError / len(data)))


if __name__ == '__main__':
    start_time = time.time()
    que = queue.Queue()
    args = createArguments()
    f = Figlet(font='slant')
    print(f.renderText('Stress Tool Radius'))
    position = 0
    threads = list()
    data = list()
    t = args.thread
    r = args.request
    requestsPerThread = 0

    if r % t == 0:
        requestsPerThread = r / t
    else:
        requestsPerThread = r / t

    if args.verbose == False:
        bar = Bar('Processing', max=t)
    for i in range(t):
        if args.type == "auth":
            t = threading.Thread(name="Thread" + str(i), target=lambda q, arg1: q.put(
                authTestFunctionLoop(args.username, args.password, args.ip, args.secret, args.timeout,
                                     requestsPerThread, args.verbose)), args=(que, 'world!'))
        else:
            t = threading.Thread(name="Thread" + str(i), target=lambda q, arg1: q.put(
                accountingTestFunction(args.username, args.password, args.ip, args.secret, args.timeout,
                                       requestsPerThread)), args=(que, 'world!'))
        position = position + 1
        threads.append(t)

    for i in threads:
        i.start()

    for i in threads:
        if args.verbose == False:
            bar.next()
        i.join()

    if args.verbose == False:
        bar.finish()

    while not que.empty():
        result = que.get()
        data.append(result)

    if que.empty():
        processData(data)

print("Time --- %s seconds ---" % (time.time() - start_time))
